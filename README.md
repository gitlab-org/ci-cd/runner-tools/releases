# GitLab Runner Releases

This is the project that kickstarts all GitLab Runner releases.

## Projects released by this project

- [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner.git)
- [GitLab Runner Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-runner.git)
- [GitLab Runner Operator](https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator.git)

For more information on how to integrate the release process tooling with other projects start with the [Releaser](https://gitlab.com/gitlab-org/ci-cd/runner-tools/releaser).

## Feature freeze

Feature freeze is to be done on a milestone's due each month. After that point onwards all the changes will be done in the next iteration.

To do a feature freeze:

1. Depending on the version of GitLab Runner that will be released create a stable branch. In this project. For example, version `15.10` of GitLab Runner means a branch named `stable-17-0`: `git pull origin main && git checkout -b stable-17-0`.
2. Edit `config.yml` to include the correct versions to be released for Runner and Chart. For example for version `17.0.0` for Runner and `0.65.0` for Chart:

```yaml
projects:
  projects:
  - name: runner
    canonical:
      url: https://gitlab.com/gitlab-org/gitlab-runner.git
      id: 250833
  - name: chart
    canonical:
      url: https://gitlab.com/gitlab-org/charts/gitlab-runner.git
      id: 6329679
  - name: operator
    canonical:
      url: https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator.git
      id: 22848448

releases:
  - name: runner
    version: v17.0.0
  - name: chart
    version: v0.65.0
    app_version: v17.0.0
  - name: operator
    version: v1.25.0
    app_version: v17.0.0
```

3. Push the branch and open the pipeline created for it: `git push origin stable-17-0 -u`
4. After the dry run jobs have passed manually run the `runner stable branch` job
5. Do the same for Chart and Operator
5. Make sure the stable branch pipelines passes

With this the feature freeze is done.

## Pulling changes after feature freeze

Exceptions for more changes to be pulled after the features freeze could be:
- Issues preventing the pipeline from succeeding
- High priority issues that are better released sooner than later

In such cases, after the feature freeze has been done, the `stable branch` jobs must be ran again.

1. Go into the relevant project stable branch and cherry-pick the **merge commits** of the MRs you wish to pull into the release with `git cherry-pick -m1 <SHA>`
2. Push the changes and rerun the `runner stable branch` job. The changelog will be correctly regenerated

**Note:** Whether the same pipeline will be reused that was used to created the feature freeze is irrelevant. As long as the `config.yml` file has correct release entries the jobs can be ran an infinite amount of times from different pipelines.

## Release process

1. Rerun all the jobs of the pipeline to make sure it's all funcioning correctly
2. Run the `runner tag dry run` job and wait for its completion
3. Run the `runner tag` job. It will start the `runner wait` job, wait for it to complete. After it passes that means the Runner tag pipeline has passed
4. Run the `chart stable branch` job and `chart tag` jobs as well as the accompaniyng dry run jobs same as Runner. 
5. You can in parallel run the `operator stable branch` job and `operator tag` jobs as well as the accompaniyng dry run jobs same as Runner. 
6. For `Operator` make sure to perform the manual actions required by its release pipeline

## Release process for patches

Patches could be issued at any given moment for the current or previous releases.

1. Choose the project you wish to release a patch for and go into that project
2. Go into the stable branch of the version that will be patched. For Runner `17.0` that will be `stable-17-0`, for Chart `0.65` - `stable-0-65`
3. Cherry-pick the **merge commits** of the MRs that you wish to be pulled into the patch with `git cherry-pick -m1 <SHA>` and push the changes. The releaser will correctly regenerate the changelog file. 
4. Go into the relevant stable branch of the **releases** project and modify the `config.yml` so that the new patches are present and versions are correctly set. For example, if we have already released Runner `17.0.0` and a patch is needed we will change the `config.yml` file as follows inside the `stable-17-0` branch:

```yaml
projects:
  projects:
  - name: runner
    canonical:
      url: https://gitlab.com/gitlab-org/gitlab-runner.git
      id: 250833
  - name: chart
    canonical:
      url: https://gitlab.com/gitlab-org/charts/gitlab-runner.git
      id: 6329679
  - name: operator
    canonical:
      url: https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator.git
      id: 22848448

releases:
  - name: runner
    version: v17.0.1
  - name: chart
    version: v0.65.1
    app_version: v17.0.1
  - name: operator
    version: v1.25.1
    app_version: v17.0.1
```

**Note** that Runner patches warrant Chart patches, but that wouldn't be always true the other way around. Use common sense. 

5. Commit the config as usual and run the pipeline. The releaser will run all the tags in their correct order and correctly update old ones or create new ones. 

## Patching older versions

If `16.11` needs to be patched while the `17.0` version has been released the `16.11` patch should live in the `17.0` milestone. The final `config.yml` config would be:

```yml
projects:
  projects:
  - name: runner
    canonical:
      url: https://gitlab.com/gitlab-org/gitlab-runner.git
      id: 250833
  - name: chart
    canonical:
      url: https://gitlab.com/gitlab-org/charts/gitlab-runner.git
      id: 6329679
  - name: operator
    canonical:
      url: https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator.git
      id: 22848448

releases:
  - name: runner
    version: v17.0.0
  - name: chart
    version: v0.65.0
    app_version: v17.0.0
  - name: operator
    version: v1.25.0
    app_version: v17.0.0

  - name: runner
    version: v16.11.1
  - name: chart
    version: v0.64.1
    app_version: v16.11.1
```

This ensures that we have perfect history of patches and their place in the history of releases. We could at any point look into what and when was released. 

**Note**: We currently don't release patches for **previous versions** of Operator, only chart and Runner.

## Example pipeline 

https://gitlab.com/ggeorgiev_gitlab/gitlab-runner-group-releases/-/pipelines/807418846